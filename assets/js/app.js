$(document).ready(function () {
    //initialize swiper when document ready
    var mySwiper = new Swiper ('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    /*Toggle mobile menu*/

    $('.site-header__button-mobile-menu').on('click', function (e) {
        e.preventDefault();
        $('body').toggleClass('menu-opened');
    });

    /*Datepicker JQuery UI*/
    $( function() {
        $("#datepicker").datepicker({
            dateFormat: "dd MM yy"
        });
    });

});